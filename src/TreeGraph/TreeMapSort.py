
 #Top K Frequent Elements
class Solution:
    # Tree Map
    # Delete, Insert O(nlog n)
    # Access O(1)
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        # Hashmap <num, count>
        numCountMap = {}
        for num in nums:# O(N)
            numCountMap[num] = numCountMap.get(num, 0) + 1
        # TreeMap Sort
        kMostNum = sorted(numCountMap, key = numCountMap.get, reverse = True)
        # TreeSort
        return kMostNum[:k]
