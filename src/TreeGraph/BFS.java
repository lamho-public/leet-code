package TreeGraph;/*
 * Created by Ho Wang Lam
 * marcohwlam@hotmail.com
 * Copyright (c) Seamless Compute 2018.
 */

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BFS {
    //pre order
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> retList = new LinkedList<List<Integer>>();
        if(root == null) return retList;
        final Queue<TreeNode> queue = new LinkedList<>(Collections.singleton(root));

        TreeNode cur;
        while (!queue.isEmpty()){
            List<Integer> subList = new LinkedList<Integer>();
            int width = queue.size();
            for(int i =0; i < width; i++){
                cur = queue.poll();
                if (cur.left != null) queue.offer(cur.left);
                if (cur.right != null) queue.offer(cur.right);
                subList.add(cur.val);
            }
            retList.add(subList);
        }
        return retList;
    }
}
