package Graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class EvaluateDiv {
    private Map<String, Map<String, Double>> Graph;
    private Set<String> Visited;
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        double[] ret = new double[queries.length];
        Graph =  new HashMap<>();
        for(int i =0; i<equations.length; i++){
            if (!Graph.containsKey(equations[i][0])){Graph.put(equations[i][0], new HashMap<>());}
            if (!Graph.containsKey(equations[i][1])){Graph.put(equations[i][1], new HashMap<>());}
            Graph.get(equations[i][0]).put(equations[i][1], values[i]);
            Graph.get(equations[i][1]).put(equations[i][0], 1.0/values[i]);
        }
        for(int i = 0; i < queries.length; i++){
            Visited = new HashSet<>();
            double ans = dfs(queries[i][0], queries[i][1], 1.0);
            ret[i] = (ans == 0)? -1 : ans;
        }
        return ret;
    }
    public double dfs(String start, String end, double value){
        if(Visited.contains(start) || !Graph.containsKey(start)) return 0.0;
        if(start.equals(end)) return 1.0;
        Visited.add(start);
        Map<String, Double> neighbours = Graph.get(start);
        if(neighbours.containsKey(end)) return value * neighbours.get(end);
        for(Map.Entry<String, Double> neighbour : neighbours.entrySet()){
            String node = neighbour.getKey();
            Double weight = neighbour.getValue() * value;
            double pathAns = dfs(node, end, weight);
            if (pathAns != 0.0) return pathAns;
        }
        return 0.0;
    }
}
