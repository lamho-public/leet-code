package Substring;

import java.util.HashMap;
import java.util.Map;

public class SubstringSearch {
//    public void findSubstring(String s, String t) {
//        int l = 0, r= 0;//sliding window
//        int[] map = new int[128];
//        int tracker;
//        int counter; // check whether the substring is valid
//        boolean counter_condition = true;
//        for(char c : t.toCharArray()) map[c]++; // init map
//        while(r < s.length()){
//            if(map[s.charAt(r++)]-- ? ){/*modify counter here*/}
//            while(counter_condition){
//                //logic model
//                /* update tracker here if finding minimum*/
//                //move l
//                if(map[s[l++]]++ ?){ /*modify counter here*/ }
//            }
//            /* update tracker here if finding maximum*/
//        }
//        //return
//    }

    public String minWindow(String s, String t) {
        int l = 0, r = 0;//sliding window
        int min_size = Integer.MAX_VALUE;
        int matched_counter = t.length();
        int startIndex = 0;
        // calculate the number of each character to be contained in S
        int[] dict = new int[128];
        for (char c : t.toCharArray()) dict[c]++;

        while(r < s.length()){
            //if right pointer char is in t, decrease count in dict
            if(dict[s.charAt(r++)]-- > 0) matched_counter--;
            // current window contains same number of the current character as in t
            while(matched_counter == 0){
                //update min
                if(r-l < min_size){
                    min_size = r - l;
                    startIndex = l ;
                }
                //if lost a char in t,
                if(dict[s.charAt(l++)]++ == 0) matched_counter++;
            }
        }
        return min_size == Integer.MAX_VALUE ? "" : s.substring(startIndex, startIndex+min_size);
    }


    public int lengthOfLongestSubstringTwoDistinct2(String s) {
        //eabeceba
        //     *
        // ^
        //Condition: at most 2 distinct char
        //find range (two points)
        //distinct char <- hash map? int[] map?
        int max_size = 0, l = 0, r = 0;
        int distinct_counter = 0;
        int[] dict = new int[128];

        while(r < s.length()){
            //if right pointer char is in t, decrease count in dict
            if(dict[s.charAt(r)] == 0) distinct_counter++;
            dict[s.charAt(r)]++;
            r++;
            // current window contains same number of the current character as in t
            while(distinct_counter > 2){
                //if lost a Distinct
                if(dict[s.charAt(l)] == 1) distinct_counter--;
                //decrese count
                dict[s.charAt(l)]--;
                //move left
                l++;
            }
            //update max
            max_size = Math.max(max_size, r - l);
        }
        return max_size;
    }
    public int lengthOfLongestSubstringTwoDistinct(String s) {
        //eeeabeceba
        //    *
        //^
        //map : e->2 a->3
        //Condition: at most 2 distinct char
        //find range (two points)
        //distinct char <- hash map? int[] map?
        if(s.length() < 1) return 0;
        Map<Character, Integer> map = new HashMap<>();
        int max = 0, l = 0, r = 0;
        while(r < s.length()){
            //dont have 2 distinct
            if(map.size() <= 2){
                // put char and index in the map
                map.put(s.charAt(r), r);
                r++;
            }
            //have more than 2 distinct
            if(map.size() > 2){
                //find left most
                int leftMost = s.length();
                for(int index : map.values()){
                    leftMost = Math.min(index, leftMost);
                }
                //remove left most from map
                map.remove(s.charAt(leftMost));
                //update window
                l = leftMost + 1;
            }
            //update max
            max = Math.max(max, r - l);
        }
        return max;
    }

}
