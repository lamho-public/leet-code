
def bSearchLeftMost(sortedNums, target):
    left, right = 0, len(sortedNums)
    index = -1
    while left <= right:
        mid = left + (right - left) // 2
        # Left side
        if sortedNums[mid] <= target:
            left = mid + 1
        else:
            right = mid - 1
        if sortedNums[mid] == target: 
            index = mid
    return index

def bSearchRightMost(sortedNums, target):
    left, right = 0, len(sortedNums)
    index = -1
    while left <= right:
        mid = left + (right - left) // 2
        # Left side
        if sortedNums[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
        if sortedNums[mid] == target: 
            index = mid
    return index
