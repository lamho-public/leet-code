class Solution:
    def __init__(self):
        self.dp = {}
    def numDecodings(self, s: str) -> int:
        if not s: return 0
        return self.reCuresiveDecode(s, 0)

    def reCuresiveDecode(self, s, i):
        if i == len(s): return 1
        if s[i] == '0': return 0
        if (len(s) - 1 == i): return 1
        if i in self.dp:
            return self.dp[i]
        ans = self.reCuresiveDecode(s, i + 1) + self.reCuresiveDecode(s, i + 2) if (int(s[i:i+2]) <= 26) else 0
        self.dp[index] = ans
        return ans
    
