package Hash;

import java.util.HashMap;
import java.util.Map;

public class LongestSubStr {
    public int lengthOfLongestSubstring(String s) {
        if(s == null || s.length() == 0) return 0;
        Map<Character, Integer> map = new HashMap<>();
        int start = 0, max = 0;
        //longest substring without repeating char
        for(int i = 0 ; i < s.length(); i++){
            char c = s.charAt(i);
            //if repeated
            if(map.containsKey(c)){
                //set start to repeated index
                start = Math.max(start, map.get(s.charAt(i))+1);
            }
            //map.put(char, index)
            map.put(c, i);
            //max update
            max = Math.max(max, i - start + 1);
        }
        return max;
    }
    public int lengthOfLongestSubstringTwoDistinct(String s) {
        if(s.length() < 1) return 0;
        HashMap<Character,Integer> index = new HashMap<Character,Integer>();
        int lo = 0;
        int hi = 0;
        int maxLength = 0;
        while(hi < s.length()) {
            if(index.size() <= 2) {
                char c = s.charAt(hi);
                index.put(c, hi);
                hi++;
            }
            if(index.size() > 2) {
                int leftMost = s.length();
                for(int i : index.values()) {
                    leftMost = Math.min(leftMost,i);
                }
                char c = s.charAt(leftMost);
                index.remove(c);
                lo = leftMost+1;
            }
            maxLength = Math.max(maxLength, hi-lo);
        }
        return maxLength;
    }

    public int lengthOfLongestSubstringTwoDistinct2(String s) {
        //eeeabeceba
        //    *
        //^
        //map : e->2 a->3
        //Condition: at most 2 distinct char
        //find range (two points)
        //distinct char <- hash map? int[] map?
        if(s.length() < 1) return 0;
        Map<Character, Integer> map = new HashMap<>();
        int max = 0, l = 0, r = 0;
        while(r < s.length()){
            //dont have 2 distinct
            if(map.size() <= 2){
                // put char and index in the map
                map.put(s.charAt(r), r);
                r++;
            }
            //have more than 2 distinct
            if(map.size() > 2){
                //find left most
                int leftMost = s.length();
                for(int index : map.values()){
                    leftMost = Math.min(index, leftMost);
                }
                //remove left most from map
                map.remove(s.charAt(leftMost));
                //update window
                l = leftMost + 1;
            }
            //update max
            max = Math.max(max, r - l);
        }
        return max;
    }
}

