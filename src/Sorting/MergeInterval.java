package Sorting;

import java.util.*;

public class MergeInterval {
    public class Interval {
        int start;
        int end;

        Interval() {
            start = 0;
            end = 0;
        }

        Interval(int s, int e) {
            start = s;
            end = e;
        }
    }

    public class IntervalComparator implements Comparator<Interval> {
        @Override
        public int compare(Interval a, Interval b) {
            return Integer.compare(a.start, b.start);
        }
    }

    public List<Interval> merge(List<Interval> intervals) {
        //O(N log(N))
        intervals.sort(new IntervalComparator());
        LinkedList<Interval> merged = new LinkedList<>();
        for (Interval inv : intervals) {
            // last end > this start than merge
            if (merged.isEmpty() || merged.getLast().end > inv.start) {
                //last end is this end
                merged.getLast().end = Math.max(merged.getLast().end, inv.end);
            } else {
                merged.add(inv);
            }
        }
        return merged;
    }

    public List<Interval> merge2(List<Interval> intervals) {
        if (intervals.size() <= 1 || intervals == null) return intervals;
        // sort start&end
        int n = intervals.size();
        int[] starts = new int[n];
        int[] ends = new int[n];
        //O(N)
        for (int i = 0; i < n; i++) {
            starts[i] = intervals.get(i).start;
            ends[i] = intervals.get(i).end;
        }
        //O(NlogN)
        Arrays.sort(starts);
        //O(NlogN)
        Arrays.sort(ends);

        // loop through O(N)
        List<Interval> res = new ArrayList<Interval>();
        for (int i = 0, j = 0; i < n; ++i) {
            if (i == n - 1 || starts[i + 1] > ends[i]) {
                res.add(new Interval(starts[j], ends[i]));
                j = i + 1;
            }
        }
        return res;
    }
}
