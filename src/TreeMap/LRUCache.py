
class LRUCache:

    def __init__(self, capacity: int):
        self.valMap = OrderedDict()
        self.capacity = capacity

    def get(self, key: int) -> int:
        # print(self.valMap)
        if key not in self.valMap: 
            return -1
        val = self.valMap[key]
        del self.valMap[key]
        self.valMap[key] = val # requeue to the back
        return val

    def put(self, key: int, value: int) -> None:
        # print(self.valMap)
        if key in self.valMap.keys():
            del self.valMap[key]
            self.valMap[key] = value
        else:
            if len(self.valMap) == self.capacity:
                self.valMap.popitem(last=False)
            self.valMap[key] = value
    
        return
