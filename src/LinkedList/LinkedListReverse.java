package LinkedList;

public class LinkedListReverse {
    public ListNode reverse_list(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode list_to_do = head.next;
        ListNode reverse_list = null;
        while (list_to_do != null) {
            ListNode temp = list_to_do;
            list_to_do = list_to_do.next;
            temp.next = reverse_list;
            reverse_list = temp;
        }
        return reverse_list;
    }
}
