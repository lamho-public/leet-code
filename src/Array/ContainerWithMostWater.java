//SlidingWindow
class Solution {
    public int maxArea(int[] height) {
        int start = 0, end = height.length - 1, max = 0;
        while(start < end){
            //update max
            max = Math.max(Math.min(height[start], height[end]) * (end - start), max);
            //shift the shroter one to center
            if(height[start] < height[end]){
                start++;
            }else{
                end--;
            }                
        }
        return max;
    }
}
