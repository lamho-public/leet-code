class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        buy = 99999999
        maxGain = 0
        for i in range(len(prices)):
            if buy > prices[i]:
                buy = prices[i]
            maxGain = max(maxGain, prices[i] - buy)
        return maxGain
        
