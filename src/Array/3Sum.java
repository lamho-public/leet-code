//O()n^2
//Two pointer
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> tripletsList = new ArrayList<>();
        for(int i = 0; i < nums.length-2; i++){
            //Skip repeat num
            if(i == 0 || nums[i - 1] != nums[i]){
                //Reset start end
                int start = i + 1, end = nums.length - 1;
                while(start < end){
                    int sum = nums[i] + nums[start] + nums[end];
                    //found
                    if(sum == 0){
                        // Add result
                        tripletsList.add((Arrays.asList(nums[i], nums[start], nums[end])));
                        //Skip same number
                        while (start < end && nums[start] == nums[start+1]) start++;
                        while (start < end && nums[end] == nums[end-1]) end--;
                        start++;
                        end--;
                    }
                    //Too big
                    else if(sum > 0){
                        end--;
                    }
                    //Too small
                    else{
                        start++;
                    }
                }
            }
        }    
        return tripletsList;
    }
}
