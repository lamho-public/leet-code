
# eg
# ecea
# lo 0 hi 2 maxLen 3
# map{e:2, c:1, b}

# eeeeeaebc

# edge cases
# 1 empty
# 2 null

# optimize
# eeeeeeekkkkkkkaaaaaaa
class Solution:
    def lengthOfLongestSubstringTwoDistinct(self, s: str) -> int:
        lo = 0
        hi = 0 # interval pointer
        distinctCountMap = {}# map<char, right most index>
        k = 2
        maxLen = 0
        while  hi < len(s):
            # increase hi while less than k distinct
            while(len(distinctCountMap) <= k and hi < len(s)):
                # save right most index of the char
                distinctCountMap[s[hi]] = hi
                hi += 1
                if (len(distinctCountMap) <= k):
                    maxLen = max(maxLen, hi - lo)
            # left most index 
            leftmost = min(distinctCountMap.values())
            # remove from 
            distinctCountMap.pop(s[leftmost])
            # lo to left most + 1
            lo = leftmost + 1
        return maxLen
            
            
