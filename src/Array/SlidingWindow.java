
public class Solution {
    public int lengthOfLongestSubstring(String s) {
        int n = s.length();
        Set<Character> set = new HashSet<>();
        int max = 0, start = 0, end = 0;
        while (start < n && end < n) {
            // try to extend the range [start, end]
            if (!set.contains(s.charAt(end))){
                set.add(s.charAt(end++));
                max = Math.max(max, end - start);
            }
            else {
                set.remove(s.charAt(start++));
            }
        }
        return max;
    }
}
