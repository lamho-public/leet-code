
class MinStack:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.minStack = [(None,float('inf'))]

    def push(self, x: int) -> None:
        self.minStack.append((x, min(x, self.minStack[-1][1])))

    def pop(self) -> None:
        self.minStack.pop()
        
    def top(self) -> int:
        return self.minStack[-1][0]
    
    def getMin(self) -> int:
        return self.minStack[-1][1]
