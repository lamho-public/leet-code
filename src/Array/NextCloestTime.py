
# Cases
# HH:MM
# H:MM
# HH:M
# next day
class Solution:
    def nextClosestTime(self, time: str) -> str:
        # get all digits
        allowed = {int(x) for x in time if x != ':'}
        closestInMin = startInMin = 60 * int(time[:2]) + int(time[3:]) # 60 * hr + min
        minDeta = 9999
        # for each possiable combination
        for h1, h2, m1, m2 in itertools.product(allowed, repeat = 4):
            hr, mins = h1*10 + h2, m1 * 10 + m2
            # if valid
            if hr < 24 and mins < 60:
                # calculate current time
                curInMin = hr * 60 + mins
                # calculate forward deta
                diff = curInMin - startInMin
                deta = (curInMin - startInMin) % (24 * 60)
                # if deta is smaller
                if(0 < deta < minDeta):
                    # update smallest
                    minDeta = deta
                    closestInMin = curInMin
                
        return "{:02d}:{:02d}".format(*divmod(closestInMin, 60))

                
                
