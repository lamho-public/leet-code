package Array;

public class MaxConsecutiveOnes {
    public int findMaxConsecutiveOnes(int[] nums) {
        int max_con = 0, p = 0, count =0;
        for(int i = 0; i< nums.length; i++){
            if (nums[i] == 1) {
                count++;
                max_con = Math.max(count, max_con);
            }
            else count = 0;
        }
        return max_con;
    }
}
